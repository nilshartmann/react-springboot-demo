package nh.contactsdemo.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import nh.contactsdemo.backend.properties.JwtProperties;

@SpringBootApplication
@EnableConfigurationProperties(JwtProperties.class)
public class ContactsBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContactsBackendApplication.class, args);
	}
}
