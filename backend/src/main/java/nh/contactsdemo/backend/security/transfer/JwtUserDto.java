package nh.contactsdemo.backend.security.transfer;

/**
 * Simple placeholder for info extracted from the JWT
 *
 * @author pascal alma
 */
public class JwtUserDto {

	private Long		id;

	private String	username;

	private String	roles;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		if (roles == null) {
			this.roles = "";
		} else {
			this.roles = roles;
		}
	}
}