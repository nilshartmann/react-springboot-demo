package nh.contactsdemo.backend.security.controller;

public class AuthenticationResponse {

	private final String	authorization;
	private final String	username;

	public AuthenticationResponse(String authorization, String username) {
		super();
		this.authorization = authorization;
		this.username = username;
	}

	public String getAuthorization() {
		return authorization;
	}

	public String getUsername() {
		return username;
	}

}
