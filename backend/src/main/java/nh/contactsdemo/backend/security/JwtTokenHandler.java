package nh.contactsdemo.backend.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import nh.contactsdemo.backend.properties.JwtProperties;
import nh.contactsdemo.backend.security.transfer.JwtUserDto;

@Component
public class JwtTokenHandler {

	private final Logger	_logger	= LoggerFactory.getLogger(getClass());

	@Autowired
	private JwtProperties	jwtProperties;

	/**
	 * Tries to parse specified String as a header containing JWT token. If
	 * successful, returns User object with username, id and role prefilled
	 * (extracted from token). If unsuccessful (token is invalid or not containing
	 * all required user properties), simply returns null.
	 *
	 * @param header
	 *          the header to parse. might be null
	 * @return the User object extracted from specified token or null if a token
	 *         is invalid or not present in the header
	 *
	 * @author pascal alma
	 */
	public JwtUserDto parseAndValidateTokenFromHeader(String header) {
		if (header == null || !header.startsWith("Bearer ")) {
			_logger.debug("No JWT token found in request headers");
			return null;
		}

		final String token = header.substring(7);

		JwtUserDto u = null;

		try {
			Claims body = Jwts.parser().setSigningKey(jwtProperties.getSecret()).parseClaimsJws(token).getBody();

			u = new JwtUserDto();
			u.setUsername(body.getSubject());
			u.setRoles((String) body.get("roles"));

			// TODO: check expiration date

		} catch (JwtException ex) {
			_logger.warn("Could not parse JWT token: " + ex, ex);
		}
		return u;
	}

	/**
	 * Generates a JWT token containing username as subject, and userId and role
	 * as additional claims. These properties are taken from the specified User
	 * object. Tokens validity is infinite.
	 *
	 * @param user
	 *          the user for which the token will be generated
	 * @return the JWT token
	 * @author pascal alma
	 */
	public String generateToken(UserDetails user) {
		Claims claims = Jwts.claims().setSubject(user.getUsername());
		claims.put("roles",
				StringUtils.collectionToCommaDelimitedString(AuthorityUtils.authorityListToSet(user.getAuthorities())));

		return Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS512, jwtProperties.getSecret()).compact();
	}

}
