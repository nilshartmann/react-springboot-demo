package nh.contactsdemo.backend.security.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import nh.contactsdemo.backend.security.JwtTokenHandler;

@RestController
@CrossOrigin(exposedHeaders = { "Authorization" })
public class LoginController {

	private final Logger					logger	= LoggerFactory.getLogger(getClass());

	@Autowired
	private AuthenticationManager	authenticationManager;

	@Autowired
	private JwtTokenHandler				jwtTokenHandler;

	@Autowired
	private UserDetailsService		userDetailsService;

	@PostMapping("/login")
	public ResponseEntity<AuthenticationResponse> login(@RequestBody AuthenticationRequest usernameAndPassword) {
		logger.info("Login '{}'", usernameAndPassword);

		final UsernamePasswordAuthenticationToken userNameAndPassword = new UsernamePasswordAuthenticationToken(
				usernameAndPassword.getUsername(), usernameAndPassword.getPassword());
		final Authentication authentication = authenticationManager.authenticate(userNameAndPassword);

		logger.info("authentication: {}", authentication);
		final UserDetails user = userDetailsService.loadUserByUsername(usernameAndPassword.getUsername());
		logger.info("User found: {}", user);

		final String jwtToken = jwtTokenHandler.generateToken(user);

		final AuthenticationResponse response = new AuthenticationResponse(jwtToken, user.getUsername());

		return ResponseEntity.ok().header("Authorization", jwtToken).body(response);
	}

}
