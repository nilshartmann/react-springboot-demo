package nh.contactsdemo.backend.security.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordGenerator {

	public static void main(String[] args) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		final String encode = encoder.encode("geheim");
		System.out.println(encode);
	}

}
