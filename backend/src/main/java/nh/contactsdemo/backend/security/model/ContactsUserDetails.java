package nh.contactsdemo.backend.security.model;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import nh.contactsdemo.backend.security.transfer.JwtUserDto;

public class ContactsUserDetails implements UserDetails {

	/**
	 * 
	 */
	private static final long															serialVersionUID	= 1L;

	private final Collection<? extends GrantedAuthority>	authorities;
	private final String																	username;
	private final String																	password;

	public ContactsUserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities) {
		this.username = username;
		this.password = password;
		this.authorities = authorities;
	}

	public static ContactsUserDetails fromUser(final nh.contactsdemo.backend.domain.model.User user) {
		return new ContactsUserDetails(user.getName(), user.getPassword(),
				Arrays.asList(new SimpleGrantedAuthority("USER")));
	}

	public static UserDetails fromJwtToken(JwtUserDto parsedUser) {
		List<GrantedAuthority> authorities = AuthorityUtils.commaSeparatedStringToAuthorityList(parsedUser.getRoles());
		return new ContactsUserDetails(parsedUser.getUsername(), null, authorities);
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.authorities;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	@Override
	public String getUsername() {
		return this.username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
