package nh.contactsdemo.backend.security.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import nh.contactsdemo.backend.domain.UserRepository;
import nh.contactsdemo.backend.domain.model.User;

@Service
public class ContactsUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		final User user = userRepository.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("User '" + username + "' not found"));

		return ContactsUserDetails.fromUser(user);
	}

}
