package nh.contactsdemo.backend.security.controller;

import javax.validation.constraints.NotNull;

public class AuthenticationRequest {

	@NotNull
	private String	username;
	private String	password;

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	@Override
	public String toString() {
		return "UsernameAndPassword [username=" + username + ", password=" + password + "]";
	}

}
