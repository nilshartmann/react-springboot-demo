package nh.contactsdemo.backend.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.GenericFilterBean;

import nh.contactsdemo.backend.security.model.ContactsUserDetails;
import nh.contactsdemo.backend.security.transfer.JwtUserDto;

public class JwtAuthenticationTokenFilter extends GenericFilterBean {

	private final Logger		_logger			= LoggerFactory.getLogger(getClass());

	private String					tokenHeader	= "Authorization";

	@Autowired
	private JwtTokenHandler	tokenHandler;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		final HttpServletRequest servletRequest = (HttpServletRequest) request;
		String header = servletRequest.getHeader(this.tokenHeader);
		final JwtUserDto parsedUser = tokenHandler.parseAndValidateTokenFromHeader(header);

		if (parsedUser != null) {
			final UserDetails userDetails = ContactsUserDetails.fromJwtToken(parsedUser);

			UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null,
					userDetails.getAuthorities());
			authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(servletRequest));

			SecurityContextHolder.getContext().setAuthentication(authentication);
		}

		chain.doFilter(request, response);
	}

}