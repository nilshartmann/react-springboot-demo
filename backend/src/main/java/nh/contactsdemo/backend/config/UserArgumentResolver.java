package nh.contactsdemo.backend.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import nh.contactsdemo.backend.domain.UserRepository;
import nh.contactsdemo.backend.domain.model.User;

public class UserArgumentResolver implements HandlerMethodArgumentResolver {

	private final Logger					_logger	= LoggerFactory.getLogger(getClass());

	private final UserRepository	userRepository;

	UserArgumentResolver(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		return User.class.isAssignableFrom(parameter.getParameterType());
	}

	@Override
	public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
			NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {

		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null) {
			_logger.debug("Authentication name '{}'", authentication.getName());
			return userRepository.findByUsername(authentication.getName()).orElseGet(() -> null);
		}

		return null;
	}

}
