package nh.contactsdemo.backend.domain.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Address {
	@Column(nullable = false)
	private int			zipcode;

	@Column(nullable = false)
	private String	city;

	public Address(int zipcode, String city) {
		super();
		this.zipcode = zipcode;
		this.city = city;
	}

	protected Address() {
		// jpa
	}

	public int getZipcode() {
		return zipcode;
	}

	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

}
