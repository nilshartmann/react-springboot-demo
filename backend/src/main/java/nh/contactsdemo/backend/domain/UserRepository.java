package nh.contactsdemo.backend.domain;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import nh.contactsdemo.backend.domain.model.User;

@Repository
public class UserRepository {

	@PersistenceContext
	private EntityManager entityManager;

	public Optional<User> findByUsername(String username) {
		final TypedQuery<User> query = entityManager.createQuery("SELECT u FROM User u WHERE u.username=:username",
				User.class);
		query.setMaxResults(1);
		query.setParameter("username", username);
		return query.getResultList().stream().findFirst();
	}

	@Transactional
	public void save(User user) {
		entityManager.persist(user);
	}
}
