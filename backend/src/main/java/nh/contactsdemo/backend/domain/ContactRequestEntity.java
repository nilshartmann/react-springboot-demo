package nh.contactsdemo.backend.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import nh.contactsdemo.backend.domain.model.Contact;

public class ContactRequestEntity {

	@NotNull
	@Size(min = 1)
	private String	name;
	private String	email;
	private int			zipcode;
	@NotNull
	@Size(min = 1)
	private String	city;

	public ContactRequestEntity() {
		super();
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public int getZipcode() {
		return zipcode;
	}

	public String getCity() {
		return city;
	}

	@Override
	public String toString() {
		return "ContactRequestEntity [name=" + name + ", email=" + email + ", zipcode=" + zipcode + ", city=" + city + "]";
	}

	public void writeTo(Contact contact) {
		contact.setName(name);
		contact.setEmail(email);
		contact.getAddress().setZipcode(zipcode);
		contact.getAddress().setCity(city);
	}
}
