package nh.contactsdemo.backend.domain.model;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import nh.contactsdemo.backend.domain.ContactRepository;
import nh.contactsdemo.backend.domain.UserRepository;

@Component

public class ModelInitializer {

	@Autowired
	private UserRepository		userRepository;

	@Autowired
	private ContactRepository	contactRepository;

	@Transactional(value = TxType.REQUIRES_NEW)
	@EventListener
	public void init(ContextRefreshedEvent event) {

		final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

		final User userKlaus = new User(1, "klaus", "Klaus Dieter", passwordEncoder.encode("geheim"));
		userRepository.save(userKlaus);
		contactRepository.save(new Contact(1, userKlaus, "Klaus Peterson", null, new Address(12345, "Hummelshausen")));
		contactRepository.save(new Contact(2, userKlaus, "Ursula Meier", null, new Address(20010, "Hamburg")));
		contactRepository.save(new Contact(3, userKlaus, "Michel Svenson", null, new Address(860, "Lönneberga")));
		contactRepository.save(new Contact(4, userKlaus, "Pierre Michel Lassoga", null, new Address(21887, "Hamburg")));

		final User userMaja = new User(2, "maja", "Maja Müller", passwordEncoder.encode("secret"));
		userRepository.save(userMaja);
		contactRepository.save(new Contact(5, userMaja, "Nils", "nils@nilshartmann.net", new Address(22356, "Hamburg")));
		contactRepository.save(new Contact(6, userMaja, "Koralle", null, new Address(22359, "Hamburg")));

	}

}
