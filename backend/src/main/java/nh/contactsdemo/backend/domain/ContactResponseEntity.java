package nh.contactsdemo.backend.domain;

import nh.contactsdemo.backend.domain.model.Contact;

public class ContactResponseEntity {
	private final String	pk;
	private final String	name;
	private final String	email;
	private final int			zipcode;
	private final String	city;

	public ContactResponseEntity(Contact contact) {
		super();
		this.pk = contact.getPk();
		this.name = contact.getName();
		this.email = contact.getEmail();
		this.zipcode = contact.getAddress().getZipcode();
		this.city = contact.getAddress().getCity();
	}

	public String getPk() {
		return pk;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public int getZipcode() {
		return zipcode;
	}

	public String getCity() {
		return city;
	}
}
