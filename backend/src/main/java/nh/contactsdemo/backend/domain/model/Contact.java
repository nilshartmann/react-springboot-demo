package nh.contactsdemo.backend.domain.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Contact {

	@Id
	private String	pk;

	@Column(nullable = false)
	private String	name;

	@Column(nullable = true)
	private String	email;

	@Embedded
	private Address	address;

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	private User		owner;

	public Contact(String pk, User owner) {
		this.pk = pk;
		this.owner = owner;
		this.address = new Address();
	}

	public Contact(int pk, User owner, String name, String email, Address address) {
		super();
		this.pk = String.valueOf(pk);
		this.owner = owner;
		this.name = name;
		this.email = email;
		this.address = address;
	}

	protected Contact() {
		// jpa
	}

	public String getPk() {
		return pk;
	}

	public void setPk(String pk) {
		this.pk = pk;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public User getOwner() {
		return owner;
	};

}
