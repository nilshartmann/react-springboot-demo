package nh.contactsdemo.backend.domain;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import nh.contactsdemo.backend.domain.model.Contact;
import nh.contactsdemo.backend.domain.model.User;

@RestController
@CrossOrigin
public class ContactsController {

	private final Logger						_logger	= LoggerFactory.getLogger(getClass());

	private final ContactRepository	contactsRepository;

	private PkGenerator							pkGenerator;

	@Autowired
	public ContactsController(ContactRepository contactsRepository, PkGenerator pkGenerator) {
		this.contactsRepository = contactsRepository;
		this.pkGenerator = pkGenerator;
	}

	@GetMapping("/contacts")
	public List<ContactResponseEntity> contacts(User user) {
		_logger.info("'##################### user {}", user);
		final List<Contact> allContactsForUser = contactsRepository.getAllContactsForUser(user);
		final List<ContactResponseEntity> entities = allContactsForUser.stream()
				.map(contact -> new ContactResponseEntity(contact)).collect(Collectors.toList());
		//
		return entities;
	}

	//
	@PostMapping("/contacts")
	@Transactional
	public ContactResponseEntity updateContact(User user, @RequestBody @Valid ContactRequestEntity contactRequestEntity) {
		_logger.info("'##################### user {}", user);
		_logger.info("'##################### contactRequestEntity {}", contactRequestEntity);

		final Contact contact = new Contact(pkGenerator.generatePk(), user);
		contactRequestEntity.writeTo(contact);
		contactsRepository.save(contact);

		return new ContactResponseEntity(contact);
	}

	@PatchMapping("/contacts/{pk}")
	@Transactional
	public ContactResponseEntity updateContact(User user, @RequestBody @Valid ContactRequestEntity contactRequestEntity,
			@PathVariable String pk) {
		_logger.info("'##################### user {}", user);
		_logger.info("'##################### pk {}", pk);
		_logger.info("'##################### contactRequestEntity {}", contactRequestEntity);

		Contact contact = contactsRepository.getByPk(user, pk);
		contactRequestEntity.writeTo(contact);
		contactsRepository.save(contact);

		return new ContactResponseEntity(contact);
	}

}
