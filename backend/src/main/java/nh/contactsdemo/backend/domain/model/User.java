package nh.contactsdemo.backend.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class User {

	@Id
	private int			pk;
	@Column(unique = true, nullable = false)
	private String	username;

	@Column(nullable = false)
	private String	fullname;

	@Column(nullable = false)
	private String	password;

	public User(int pk, String name, String fullname, String password) {
		super();
		this.pk = pk;
		this.username = name;
		this.fullname = fullname;
		this.password = password;
	}

	User() {
		// jpa
	}

	public int getPk() {
		return pk;
	}

	public void setPk(int pk) {
		this.pk = pk;
	}

	public String getName() {
		return username;
	}

	public void setName(String name) {
		this.username = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFullname() {
		return fullname;
	}

}
