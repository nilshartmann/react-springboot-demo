package nh.contactsdemo.backend.domain;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import nh.contactsdemo.backend.domain.model.Contact;
import nh.contactsdemo.backend.domain.model.User;

@Repository
public class ContactRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@Transactional(readOnly = true)
	public List<Contact> getAllContactsForUser(User owner) {
		final TypedQuery<Contact> query = entityManager.createQuery("SELECT c FROM Contact c where c.owner=:owner",
				Contact.class);
		query.setParameter("owner", owner);
		return query.getResultList();
	}

	@Transactional
	public void save(Contact contact) {
		entityManager.persist(contact);
	}

	public Contact getByPk(User owner, String pk) {
		final TypedQuery<Contact> query = entityManager
				.createQuery("SELECT c FROM Contact c where c.pk=:pk AND c.owner=:owner", Contact.class);
		query.setParameter("pk", pk);
		query.setParameter("owner", owner);

		return query.getSingleResult();

	}

}
