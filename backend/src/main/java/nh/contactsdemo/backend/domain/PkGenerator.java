package nh.contactsdemo.backend.domain;

import java.util.UUID;

import org.springframework.stereotype.Component;

/** TODO */
@Component
public class PkGenerator {

	public String generatePk() {
		return UUID.randomUUID().toString();
	}

}
