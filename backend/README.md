# Run the example

* In folder backend: `./gradlew eclipse`
* Import `backend` project in eclipse (File -> Import... -> General -> Existing projects into workspace)
* On `nh.contactsdemo.backend` from context menu: ` 




# Example

## Credits: JWT for Spring Security

The JWT adapter for spring security (package `nh.contactsdemo.backend.security`) is taken from examples by:
* [Pascal Alma](https://pragmaticintegrator.wordpress.com/): https://gitlab.com/palmapps/jwt-spring-security-demo. Please see also his related [blog post at dzone](https://dzone.com/articles/validating-jwt-with-spring-boot-and-springsecurity)
* [Stephan Zerhusen](https://twitter.com/stzerhus) https://github.com/szerhusenBC/jwt-spring-security-demo 

I've just mixed these two examples and added my own stuff.
 



 

